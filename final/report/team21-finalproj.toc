\contentsline {section}{\numberline {1}Overall Strategy and Design}{2}{section.1}
\contentsline {subsection}{\numberline {\textbf {1.a}}Design}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {\textbf {1.b}}Robot Strategy}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {\textbf {1.c}}Experiment Strategy}{4}{subsection.1.3}
\contentsline {section}{\numberline {2}The Code}{5}{section.2}
\contentsline {subsection}{\numberline {\textbf {2.a}}Flowchart}{5}{subsection.2.1}
\contentsline {subsubsection}{\numberline {\textbf {2.a}.1}Functions}{5}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {\textbf {2.a}.2}Tasks}{13}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {\textbf {2.a}.3}Main}{15}{subsubsection.2.1.3}
\contentsline {subsection}{\numberline {\textbf {2.b}}Pseudocode}{16}{subsection.2.2}
\contentsline {subsubsection}{\numberline {\textbf {2.b}.1}Functions}{16}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {\textbf {2.b}.2}Tasks}{19}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {\textbf {2.b}.3}Main}{19}{subsubsection.2.2.3}
\contentsline {section}{\numberline {3}Robot Performance}{20}{section.3}
\contentsline {subsection}{\numberline {\textbf {3.a}}Sensors}{20}{subsection.3.1}
\contentsline {subsection}{\numberline {\textbf {3.b}}Turing Test Results}{21}{subsection.3.2}
\contentsline {section}{\numberline {4}Creativity}{22}{section.4}
\contentsline {subsection}{\numberline {\textbf {4.a}}Design}{22}{subsection.4.1}
\contentsline {subsection}{\numberline {\textbf {4.b}}Software}{22}{subsection.4.2}
\contentsline {section}{\numberline {5}Conclusion}{22}{section.5}
