\normalfont\documentclass[a4paper, 12pt]{article}
\usepackage[a4paper,total={6.5in, 10in}]{geometry}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{comment}
\usepackage[colorlinks]{hyperref}
\usepackage{wrapfig}
\hypersetup{linkcolor=black}
\hypersetup{urlcolor=blue}

\usepackage{verbatim}
\usepackage{listings}
\usepackage{color}

\definecolor{deepgreen}{rgb}{0,0.6,0}
\definecolor{orange}{rgb}{1,0.5,0}
\definecolor{purple}{RGB}{102,0,102}

\renewcommand{\thesubsection}{\textbf{\thesection.\alph{subsection}}}
\newcommand{\tabitem}{$\bullet$  }
\setlength\parindent{0pt}

% C environment
\lstnewenvironment{ccode}[1][]{
	\lstset{
	  language=C,
	  backgroundcolor=\color{white},
	  otherkeywords={task, setMotorSpeed, moveMotorTarget, waitUntilMotorStop, sleep},
	  keywordstyle=\color{blue},
	  emphstyle=\color{purple},
	  stringstyle=\color{deepgreen},
	  commentstyle=\color{deepgreen},
	  showstringspaces=false,
	  %frame=tblr,		% Turn on and off box frame around code
	  #1	% mostly to be used for emph={Class, Function, etc.}
	}
}{}


\title{Lab 4\\Grab It and Take It and Move It and Shake It}
\author{Bohyun Hwang (25868043)\\Heliodoro Tejeda (53604946)}
\date{\today}

\begin{document}

\maketitle \tableofcontents

\clearpage

\section{Introduction}

Without sensors, robots are essentially flying blind. They are restricted to executing exactly what they have been programmed to do. This can range from fairly simple (routine tasks), which are very simple, to more complex tasks in which a robot needs to follow a strict path, in which case, any deviation in the course will cause complete chaos and failure. To fix this problem, we give our robots sensors. This will essentially give them ``eyes'', preventing them from flying blind.\\
\\
It is important to note that the term ``eyes'' here does not directly mean that we are giving our robots a pair of sensors in which they will be able to process visual information. Rather, it means that they are given sensors that will allow them to sense their surroundings and make decisions. One very important type of sensor, when lacking the resources to allow your robot to pick up visual information, is sonar. Much like bats, with sonar, your robot will be able to sense what is around it, preventing it from crashing. In addition to not crashing, this can give our robot the ability to maneuver around obstacles, travel a set distance until it reaches an obstacle then do something, and much more.\\
\\
In addition to sensors, we also want our robots to accomplish other tasks. As opposed to just moving around, we would like our robot to interact with its surroundings. To do this, we give the robot a limb. To start simple, with the LEGO Mindstorms kit, we give the robot a single arm. This allows the robot to pick up and move items. Having both powers, sensors and limbs allows robots to move about their environment and interact with it.\\
\\
For this lab, we added sonar to our robots with the aim of having it approach an object, pick it up, and return to its starting point. Having sonar, our robot is able to sense how close it gets to this object and with its arm, it is able to pick up the object.

\section{Specific Aims}

In this lab, our goal was to utilize the sonar sensor given with our LEGO Mindstorms kit, pick up an object, and return it to our starting position. The first part of this task is to understand how the sonar system worked, and to figure out how accurate the sonar for our robot is. Following that, we needed to work the arm, allowing it to move up and down as needed. Finally, the objective was to put it all together, and grab and go.

\section{Materials}

$\bullet$ Lego Mindstorms Basic Robot\\
$\bullet$ LEGO Mindstorms Ultrasonic Sensor\\
$\bullet$ LEGO Mindstorms Color Box\\
$\bullet$ LEGO Mindstorms Mechanical Arm\\
$\bullet$ RobotC Software\\
$\bullet$ Pencil Case

\section{Methods}

To begin this lab, we first had to attach our Ultrasonic Sensor to our robot. To do this, we started with our LEGO Mindstorms basic robot (with the Color Sensor attached, from last weeks lab) and followed the instructions on pages 42-47 of our LEGO Mindstorms construction pamphlet. Next, we followed the instructions on pages 4-6 of the pamphlet and built the LEGO Mindstorms Color Box. Once this was completed, we now had our Ultrasonic Sensor attached and could test it.\\
\\
To test it, we used the Port View panel on our robot. This panel allowed us to use the sonar sensor without having to program the robot specifically. Once in the Port View panel of our robot, we placed several different items in front of the sensor and recorded the distances (in centimeters) that were displayed on the panel. We did this several times with two different object, one was the color box we built, the second was a pencil case that we owned. We placed our objects at known distances from the sonar sensor and recorded the distances that the sonar picked up (these distances were displayed on the Port View panel in centimeters). To account for variability, we tested the sonar at 8 different distances and two different times for each distance. These measurements can be found in the Results section.\\
\\
After testing our sonar system, we then attached the Mechanical Arm to our robot. To do this, we followed the instructions on pages 54-68 of our LEGO Mindstorms construction pamphlet. For repetition purposes, it is important to note that one must push the small red piece all the way into the motor shaft. With the arm then attached, we now had our robot with a sonar sensor and a mechanical arm, in addition to a color box that we could use as an object to pick up.\\
\\
Next, we decided to divide the lab into simple subparts. We split the lab into three parts. The first subpart was to program the robot (using the sonar sensor) to approach an object and stop 4 centimeters in front of the object. The second subpart was to lower the arm a set distance that it could wrap around the color box and be able to drag it. The final subpart was to incorporate a timer into our program. This would allow us to time how long our robot moved forward. With this, we could use the set time to move the same distance backwards, thus, returning us to our starting point. Once we had all three subparts completed, we combined them into one program that would execute the entire lab objective. A flowchart and pseudocode for our implementation is provided below.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=\textwidth, height=0.2\textheight]{pics/flowchart.png}
	\caption{Flowchart}
\end{figure}

\pagebreak

\begin{figure}[!ht]
	\centering
	\begin{ccode}
Clear/Reset Timer

while (Object > 4 cm away)

	Move Robot Forward

end while

Stop Robot
Set Time Elapsed Variable
Lower Robot Arm
Clear/Reset Timer

while (Current Timer < Time Elapsed Var)

	Move Robot Backwards

end while

Stop Robot
Raise Robot Arm

Dance Baby Dance!!!
	\end{ccode}
	\caption{Pseudocode}
\end{figure}

\section{Results}

Before actually implementing any code, we had to test our sonar system. As described earlier, we used the Port View panel of our robot to take various measurements. These measurements are provided below.

\begin{figure}[!ht]
	\centering
		\begin{tabular}{|c|c|c|c|c|c|}
			\hline
			 & D1 & D2 & D3 & D4 & D5 \\ \hline
			Measurement 1 (M1) & 3.8 & 6.7 & 10.9 & 15.4 & 19.1 \\ \hline
			Measurement 2 (M2) & 4.0 & 6.7 & 11.4 & 15.8 & 19.1 \\ \hline
			Real Measurement & 3.81 & 6.35 & 10.795 & 15.875 & 19.05 \\
			\hline
		\end{tabular}
	\caption{Measurements Using Color Box}
\end{figure}

\begin{figure}[!ht]
	\centering
		\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|}
			\hline
			 & D1 & D2 & D3 & D4 & D5 & D6 & D7 & D8 & D9 \\ \hline
			M1 & 3.8 & 6.5 & 10.3 & 14.9 & 19.1 & 32.1 & 63.5 & 92.5 & 124.2 \\ \hline
			M2 & 4.0 & 6.7 & 10.1 & 15.6 & 19.1 & 32.8 & 63.9 & 93.1 & 125.6 \\ \hline
			Real & 3.81 & 6.35 & 10.795 & 15.875 & 19.05 & 30.48 & 60.96 & 91.44 & 121.92 \\
			\hline
		\end{tabular}
	\caption{Measurements Pencil Case}
\end{figure}

After testing our sensor, we could see that there was some variability within its own measurements. In addition, there was inconsistencies with different objects at the same distance. Thus, we realized that we needed to thoroughly test our robot's stopping distance from object while in motion.\\
\\
This was the first subpart of the lab. And, as it turns out, our robot was able to, while in motion, detect an object that was 4 cm away from it and stop consistently. With this, we were able to then program the arm to go down far enough for it to wrap around the colored box. After accomplishing that subtask, we then accomplished our final subtask of incorporating a timer into our program. This timer was very useful and very accurate. We ran several test before demonstrating our finished lab. All tests were very accurate. The robot would move forward until it reached its object, then move backwards for the same amount of time that it had moved forward.\\
\\
Once all three parts were completed, combining the program into one and making the robot do the three things we wanted it to do was easy. After completing the task, our robot then celebrated by dancing it's little mechanical heart out!

\section{Discussion}

The fact that we simplified the entire lab into subtasks, accomplishing each subtask individually, then combining it into one was great. With this, we were able to divide the complexity of the task into smaller more easily accomplished tasks. The sonar worked better than expected. Compared to the initial test we ran on the Port View, where the measured distances were a bit off and inconsistent; the measurements taken by the system while the robot was in motion stopping 4 cm away from an object worked great. The hardest part of the lab was figuring out how to utilize the timer system. However, once we were able to find ample documentation, we were able to work it out and integrate it into our code.\\
\\
Finally, the color box is just small enough to fit inside of the robot's mechanical arm. Thus, the robot needs approach the box at exactly the right angle to be able to lower its arms around the box and grab it. We found it difficult at times to do this, as the robot was just slightly off (to the left or to the right) in which case, the arm would land on top of the box raising one of the robot's wheels off of the ground. When this happened, the robot was then unable to move in a predictable manner. Fortunately, there were enough instances when everything went correctly and we were able to complete the lab.\\
\\
In the future, we would like to figure out how to go about fixing this. Making slight adjustments after a move has been executed to correct these slight errors. Having the ability to do that will give us and our robot more power and greater accuracy.

\section{Lab Questions}

To move the robot's mechanical arm we used a motor encoder. Before finalizing how far down we would actually move the arm, we took several different measurements at different degrees to see exactly how far the arm would move. We finally settled at 150 degrees for both up and down. That seemed about right for our robot to move its arm down to the ground (from the top) and be able to move it (raise the arm all the way to the top) again.\\
\\
As described throughout the report, there were several issues while using the ultrasonic sensor. However, most of the issues came from the initial testing before programming anything. Most inconsistencies lied in our stationary object measurements. Once we programmed our robot to move forward and stop a set distance away from an object, 9/10 times it worked fine. The 1 time it did not work fine, was when the sensor would go through the hole that is in the color box, thus not picking up that the object was there in the first place. In practice, this would be a huge problem since not all objects will be completely solid.\\
\\
The construction of the arm and attaching the sensor went smoothly. Once everything was assembled, we were able to use them just fine. Following the directions thoroughly and ensuring that you do each step correctly is important.

\end{document}
