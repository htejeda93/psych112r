\normalfont\documentclass[a4paper, 12pt]{article}
\usepackage[a4paper,total={6.5in, 10in}]{geometry}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{comment}
\usepackage[colorlinks]{hyperref}
\usepackage{wrapfig}
\hypersetup{linkcolor=black}
\hypersetup{urlcolor=blue}

\usepackage{verbatim}
\usepackage{listings}
\usepackage{color}

\definecolor{deepgreen}{rgb}{0,0.6,0}
\definecolor{orange}{rgb}{1,0.5,0}
\definecolor{purple}{RGB}{102,0,102}

\renewcommand{\thesubsection}{\textbf{\thesection.\alph{subsection}}}
\newcommand{\tabitem}{$\bullet$  }
\setlength\parindent{0pt}

% C environment
\lstnewenvironment{ccode}[1][]{
	\lstset{
	  language=C,
	  backgroundcolor=\color{white},
	  otherkeywords={task, setMotorSpeed, moveMotorTarget, waitUntilMotorStop, sleep},
	  keywordstyle=\color{blue},
	  emphstyle=\color{purple},
	  stringstyle=\color{deepgreen},
	  commentstyle=\color{deepgreen},
	  showstringspaces=false,
	  %frame=tblr,		% Turn on and off box frame around code
	  #1	% mostly to be used for emph={Class, Function, etc.}
	}
}{}


\title{Lab 5\\Emotional (Sensory Responsive) Vehicles}
\author{Ashley Chao (10511465)\\Heliodoro Tejeda (53604946)}
\date{\today}

\begin{document}

\maketitle \tableofcontents

\clearpage

\section{Introduction}

Braitenberg vehicles, are simple vehicles for which motion is directly connected to sensory input. Because of this connection, the vehicles seem like intelligent agents as they react differently to different stimuli (in this case, the stimulus is light). As an example, the same vehicle (Vehicle 2) will seem to demonstrate aggression or fear based on the sensory-motor connections. Fear will be demonstrated when the left sensor is connected to the left motor and the right sensor is connected to the right motor. In this case, when sensory input is higher on one side of the vehicle, it will turn away from the stimulus. However, when the sensory-motor connections are crossed (left sensor is connected to the right motor and right sensor is connected to the left motor), aggression is shown. Although nothing has changed with the implementation of how the vehicle operates, the simple rewiring will demonstrate different actions.\\
\\
These type of vehicles are purely mechanical as there is no information processing done to determine actions. Sensory input is directly wired to movement which gives the vehicle the illusion of cognitive abilities. With this, our goal was to recreate Braitenberg's vehicles with our own robots and create these type of agents. We want to demonstrate different types of cognitive abilities based on the rewiring of motor controls. By doing this, we can show that the same program will demonstrate different types of cognitive abilities based on wiring.


\section{Specific Aims}

The objective of this lab is to recreate Braitenberg Vehicles 2 and 3 using the Lego Mindstorms kit. For vehicle two, the goal is to create a single program that will demonstrate both fear and aggression. This should be done by swapping the wiring of the motors and not changing anything in our code. For vehicle three, the goal is again to create a single program, however, this time demonstrating love and exploration. Again, these different actions should be demonstrated with the same program by only changing the wiring to the motors.

\section{Materials}

$\bullet$ Lego Mindstorms Basic Robot\\
$\bullet$ LEGO Mindstorms Color Sensor Block (2x)\\
$\bullet$ RobotC Software\\
$\bullet$ JLKcolor.c (sample program for the color)\\
$\bullet$ Flashlight\\
$\bullet$ Lamp

\pagebreak

\section{Methods}

To begin this lab we first attached two color sensors to the front of the robot, one on each side. To accomplish this, we followed the instructions given to us from page 73-79 of our LEGO Mindstorm pamphlet.\\
\\
Next, we opened up the ROBOTC software to start programming. We worked off of the sample code, \href{https://eee.uci.edu/17w/68220/examples/JLKcolor.c}{JLKcolor.c} provided to us by the professor to create a test program with which we could test our left and right sensors. Instead of having the robot sense color we changed the setting to ambient on the ROBOT C software. We then wrote a few lines of code to display the light values on the screen. Using this program we were able to run tests on the sensor to see how accurate it was. After seeing that the sensors were able to tell which side was picking up more light we were ready to start our program.

\subsection{Vehicle 2}

For vehicle 2a we decided to use a while true statement so that the robot would run our program infinitely (until we stopped it). We wanted the motor speed of our robot to be directly correlated to the values from the corresponding sensor. So when one sensor picks up more light then the corresponding motor speed would increase with it. This would cause the robot to turn away from the light source. In order to do this we set variables for the light values that the robot picks up on the left sensor and the right sensor. We then set the variables as the motor speeds for the motors on the same side as the sensor. To finish it off we programmed our robot to display the values on the screen. The values were the variables that we set earlier, this means that the left light sensor and left motor speed displayed on the screen came from the same variable and the right sensor and right motor speed came from the other variable.\\

\begin{figure}[!ht]
	\centering
	\begin{ccode}
Setup Sensors/Motors

while (true)
{
	Set Llightvalue to the value picked up from left sensor
	Set Rlightvalue to the value picked up from the right sensor

	Set motor speed for left motor to Llightvalue
	Set motor speed for right motor to Rlightvalue

	Display left sensor value (Llightvalue)
	Display left motor speed (Llightvalue)
	Display right motor value (Rlightvalue)
	Display right motor speed (Rlightvalue)
}
	\end{ccode}
	\caption{Pseudocode}
\end{figure}
For vehicle 2b we simply switched the cables for the motors so that it corresponded to the opposite sensor as vehicle 2a. By doing this, when the light sensor value is lower, the corresponding motor on the opposite side will be faster and so the robot will move towards the light source.


\subsection{Vehicle 3}

For vehicle 3a we wanted the motor speed to be the inverse of the light sensor values. In order to do this we first switched the motor wires back so each motor corresponds with the sensor on the same side of the robot. We then used the same code from vehicle 2a and manipulated the motor speeds to be the inverse of the values picked up by the light sensors. To do this, we subtracted the variables from 100 to get the inverse value for the motor speeds. This inverse of motor speed caused the robot to slow down when light intensity was high and speed up when light intensity was low.\\

\begin{figure}[!ht]
	\centering
	\begin{ccode}
Setup Sensors/Motors
while (true)
{
	Set Llightvalue to the value picked up from left sensor
	Set Rlightvalue to the value picked up from the right sensor

	Set motor speed for left motor to (100- Llightvalue)
	Set motor speed for right motor to (100- Rlightvalue)

	Display left sensor value (Llightvalue)
	Display left motor speed (100 - Llightvalue)
	Display right motor value (Rlightvalue)
	Display right motor speed (100 - Rlightvalue)
}
	\end{ccode}
	\caption{Pseudocode}
\end{figure}\\
\\
For vehicle 3b we switched the motor wires again so that the motors would correspond with the sensor on the other side of the robot.


\section{Results}

We first tested out the color sensor using the test code we wrote. The code was very simple, it made the robot display the values from both the left and right sensors at the same time. We took three measurements at each distance from different positions (left means we shined the light closer to the left sensor, right means we shined the light closer to the right sensor, and center means we shined the light in between both sensors). We took 12 measurements in total for each sensor at 4 different distances (2, 6, 12, and 24 inches). The values that we measured are displayed below.

\begin{figure}[!ht]
	\centering
		\begin{tabular}{|c|c|c|c|c|c|c|}
			\hline
			\textbf{Position} & \textbf{Distance (in)} & \textbf{Trial 1} & \textbf{Trial 2} & \textbf{Trial 3} & \textbf{Mean ($\mu$)} & \textbf{SD ($\sigma$)}\\ \hline
			Left & 2 & 55 & 55 & 54 & 54.667 & 0.471 \\ \hline
			Center & 2 & 7 & 3 & 4 & 4.667 & 1.700 \\ \hline
			Right & 2 & 2 & 2 & 2 & 2.000 & 0.000 \\ \hline
			Left & 6 & 33 & 30 & 32 & 31.667 & 1.247 \\ \hline
			Center & 6 & 28 & 25 & 25 & 26.000 & 1.414 \\ \hline
			Right & 6 & 12 & 12 & 11 & 11.667 & 0.471 \\ \hline
			Left & 12 & 21 & 20 & 21 & 20.667 & 0.471 \\ \hline
			Center & 12 & 19 & 17 & 17 & 17.667 & 0.943 \\ \hline
			Right & 12 & 15 & 12 & 13 & 13.333 & 1.247 \\ \hline
			Left & 24 & 11 & 10 & 10 & 10.333 & 0.471 \\ \hline
			Center & 24 & 11 & 9 & 9 & 9.667 & 0.943 \\ \hline
			Right & 24 & 9 & 7 & 8 & 8.000 & 0.816 \\ \hline
		\end{tabular}
	\caption{Left Color/Light Sensor}
\end{figure}

\pagebreak

\begin{figure}[!ht]
	\centering
		\begin{tabular}{|c|c|c|c|c|c|c|}
			\hline
			\textbf{Position} & \textbf{Distance (in)} & \textbf{Trial 1} & \textbf{Trial 2} & \textbf{Trial 3} & \textbf{Mean ($\mu$)} & \textbf{SD ($\sigma$)}\\ \hline
			Left & 2 & 4 & 4 & 4 & 4.000 & 0.000 \\ \hline
			Center & 2 & 6 & 6 & 8 & 6.667 & 0.943 \\ \hline
			Right & 2 & 55 & 54 & 54 & 54.333 & 0.471 \\ \hline
			Left & 6 & 10 & 16 & 12 & 12.667 & 2.494 \\ \hline
			Center & 6 & 24 & 28 & 29 & 27.000 & 2.160 \\ \hline
			Right & 6 & 33 & 31 & 32 & 32.000 & 0.816 \\ \hline
			Left & 12 & 17 & 17 & 16 & 16.667 & 0.471 \\ \hline
			Center & 12 & 19 & 19 & 20 & 19.333 & 0.471 \\ \hline
			Right & 12 & 22 & 20 & 21 & 21.000 & 0.816 \\ \hline
			Left & 24 & 10 & 10 & 10 & 10.000 & 0.000 \\ \hline
			Center & 24 & 9 & 11 & 10 & 10.000 & 0.816 \\ \hline
			Right & 24 & 12 & 11 & 11 & 11.333 & 0.471 \\ \hline

		\end{tabular}
	\caption{Right Color/Light Sensor}
\end{figure}

We shined our phone flash light at the sensors at 4 different distances. We also tested out shining at the right sensor, left sensor and right down the middle. This way we were able to see how much light each sensor was picking up when the light was concentrated in one area. The data shows that the color sensor was measuring light relatively consistently. The values varied only slightly per each round and the difference was mostly caused by human error. We tried our best to measure out where to shine the light at, but it definitely was not at the same spot every time so that could cause the small variance.%\\
%\\
%The lab did not require our robot to be very precise in its measurement of light since it was mostly comparing light values between sensors. Since the robot was definitely able to tell which sensor was picking up more light we were able to program our robot with not problems caused by the sensors.

\section{Discussion}

Overall this lab was very successful. We were able to breeze through it with almost no difficulty. Our code worked exactly as we expected it to. Since the code was relatively simple there were not a lot of opportunities for mistakes. The only difficulty we really had was determining how to implement the difference between vehicle 2 and vehicle 3. However, once we wrote down a few data points, we were easily able to figure out the simple formula of $100 -\ light\ sensor\ value$. One thing we would do differently is to scale the speed of the robot. Since we set the motor speed as the light sensor values, the speed could get pretty fast and make it hard to observe the behavior of the robot. In order to fix this problem, we could scale the speed so that it doesn’t move too fast. This was especially evident in Vehicle 3. \\
\\
Finally, in this lab, we learned that sensor input can be directly connected to motor speeds. This allows for their to be a direct connection between both systems. With this, there is no information processing that goes on to determine what actions to take based on sensory input. Rather, the vehicle continues to move based on its sensory input. This simplifies the programming implementation of the robot.



\end{document}
