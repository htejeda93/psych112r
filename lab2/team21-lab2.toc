\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}Specific Aims}{2}{section.2}
\contentsline {section}{\numberline {3}Materials}{2}{section.3}
\contentsline {section}{\numberline {4}Methods}{2}{section.4}
\contentsline {subsection}{\numberline {\textbf {4.a}}Part 1}{2}{subsection.4.1}
\contentsline {subsubsection}{\numberline {\textbf {4.a}.1}Move Robot}{3}{subsubsection.4.1.1}
\contentsline {paragraph}{Forward - Fast:}{3}{section*.2}
\contentsline {paragraph}{Forward - Slow:}{3}{section*.3}
\contentsline {paragraph}{Backwards - Fast:}{3}{section*.4}
\contentsline {paragraph}{Backwards - Slow:}{3}{section*.5}
\contentsline {subsubsection}{\numberline {\textbf {4.a}.2}Spin Robot}{4}{subsubsection.4.1.2}
\contentsline {paragraph}{Spin - Fast:}{4}{section*.6}
\contentsline {paragraph}{Spin - Slow:}{4}{section*.7}
\contentsline {subsection}{\numberline {\textbf {4.b}}Part 2}{4}{subsection.4.2}
\contentsline {subsubsection}{\numberline {\textbf {4.b}.1}Move Robot}{5}{subsubsection.4.2.1}
\contentsline {paragraph}{Forward - Fast:}{5}{section*.8}
\contentsline {paragraph}{Forward - Slow:}{5}{section*.9}
\contentsline {paragraph}{Backwards - Fast:}{5}{section*.10}
\contentsline {paragraph}{Backwards - Slow:}{5}{section*.11}
\contentsline {subsubsection}{\numberline {\textbf {4.b}.2}Spin Robot}{6}{subsubsection.4.2.2}
\contentsline {paragraph}{Spin - Fast:}{6}{section*.12}
\contentsline {paragraph}{Spin - Slow:}{6}{section*.13}
\contentsline {subsection}{\numberline {\textbf {4.c}}Part 3}{6}{subsection.4.3}
\contentsline {section}{\numberline {5}Results}{7}{section.5}
\contentsline {subsection}{\numberline {\textbf {5.a}}Part 1}{7}{subsection.5.1}
\contentsline {subsubsection}{\numberline {\textbf {5.a}.1}Move Robot}{7}{subsubsection.5.1.1}
\contentsline {paragraph}{Forward - Fast:}{7}{section*.14}
\contentsline {paragraph}{Forward - Slow:}{7}{section*.16}
\contentsline {paragraph}{Backwards - Fast:}{7}{section*.18}
\contentsline {paragraph}{Backwards - Slow:}{8}{section*.20}
\contentsline {subsubsection}{\numberline {\textbf {5.a}.2}Spin Robot}{8}{subsubsection.5.1.2}
\contentsline {paragraph}{Spin - Fast:}{8}{section*.22}
\contentsline {paragraph}{Spin - Slow:}{8}{section*.24}
\contentsline {subsection}{\numberline {\textbf {5.b}}Part 2}{9}{subsection.5.2}
\contentsline {subsubsection}{\numberline {\textbf {5.b}.1}Move Robot}{9}{subsubsection.5.2.1}
\contentsline {paragraph}{Forward - Fast:}{9}{section*.26}
\contentsline {paragraph}{Forward - Slow:}{9}{section*.28}
\contentsline {paragraph}{Backwards - Fast:}{9}{section*.30}
\contentsline {paragraph}{Backwards - Slow:}{10}{section*.32}
\contentsline {subsubsection}{\numberline {\textbf {5.b}.2}Spin Robot}{10}{subsubsection.5.2.2}
\contentsline {paragraph}{Spin - Fast:}{10}{section*.34}
\contentsline {paragraph}{Spin - Slow:}{10}{section*.36}
\contentsline {subsection}{\numberline {\textbf {5.c}}Part 3}{10}{subsection.5.3}
\contentsline {section}{\numberline {6}Discussion}{11}{section.6}
\contentsline {section}{\numberline {7}Lab Questions}{11}{section.7}
