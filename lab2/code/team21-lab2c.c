// Psych 112R - Cognitive Robotics
//	Lab 2 - Part (c)
//
//		TEAM 21
//		Author 1: Helio Tejeda
//		Author 2: Bohyun Hwang

// Function to move robot (by degree rotation)
void moveRobot(int moveDegree, int speed)
{
	// The speed input determines the direction
	// the robot will move
	//	     positive speed -> forward
	//		   negative speed -> backwards
	moveMotorTarget(motorB, moveDegree, speed);
	moveMotorTarget(motorC, moveDegree, speed);
	waitUntilMotorStop(motorB);
	waitUntilMotorStop(motorC);
}

// Function to spin the robot (by degree rotation) LEFT
void spinRobotLeft(int moveDegree, int speed)
{
	// The speed input will be positive for one
	// direction and negative for the other.
	// This will cause the robot to spin in place.
	moveMotorTarget(motorB, moveDegree, -speed);
	moveMotorTarget(motorC, moveDegree, speed);
	waitUntilMotorStop(motorB);
	waitUntilMotorStop(motorC);
}

// Function to spin the robot (by degree rotation) RIGHT
void spinRobotRight(int moveDegree, int speed)
{
	// The speed input will be positive for one
	// direction and negative for the other.
	// This will cause the robot to spin in place.
	moveMotorTarget(motorB, moveDegree, speed);
	moveMotorTarget(motorC, moveDegree, -speed);
	waitUntilMotorStop(motorB);
	waitUntilMotorStop(motorC);
}

task main()
{
	int speed = 70;	// fast
	//int speed = 20;	// slow

	// Move robot forward 2 squares at 100%
	moveRobot(720*2, speed);

	// Spin Robot 45 degrees left at 100%
	spinRobotLeft(120*1.2, speed);	// slow
	//spinRobotLeft(120*1.2, speed);	// fast

	// Move robot forward 1 square diag at 100%
	moveRobot(720*1.25, speed);

	// Spin Robot 45 degrees right at 100%
	spinRobotRight(120*1.48, speed);	// slow
	//spinRobotRight(120*1.42, speed);  // fast

	// Move robot forward 6 squares at 100%
	moveRobot(720*5.2, speed);

	// Spin Robot 90 degrees left at 100%
	spinRobotLeft(120*2.1, speed);

	// Move robot forward 2 squares at 100%
	moveRobot(720*1.5, speed);

}
