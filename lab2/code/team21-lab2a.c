// Psych 112R - Cognitive Robotics
//	Lab 2 - Part (a)
//
//		TEAM 21
//		Author 1: Helio Tejeda
//		Author 2: Bohyun Hwang

// Function to move robot
void moveRobot(int speed, int moveTime)
{
	// The speed input determines the direction
	// the robot will move
	//	     positive speed -> forward
	//		   negative speed -> backwards
	setMotorSpeed(motorB, speed);
	setMotorSpeed(motorC, speed);
	sleep(moveTime);
}

// Function to spin the robot in place
void spinRobot(int speed, int moveTime)
{
	// The speed input will be positive for one
	// direction and negative for the other.
	// This will cause the robot to spin in place.
	setMotorSpeed(motorB, speed);
	setMotorSpeed(motorC, -speed);
	sleep(moveTime);
}

task main()
{

	// Move robot forward at 100% for 2 sec
	//moveRobot(100, 2000);

	// Move robot forward at 10% for 2 sec
	//moveRobot(10, 2000);

	// Move robot backward at 100% for 2 sec
	//moveRobot(-100, 2000);

	// Move robot backward at 10% for 2 sec
	//moveRobot(-10, 2000);

	// Spin Robot at 100% for 2 sec
	//spinRobot(100, 2000);

	// Spin robot at 10% for 2 sec
	spinRobot(10, 2000);

}
