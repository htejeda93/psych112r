// Psych 112R - Cognitive Robotics
//	Lab 2 - Part (b)
//
//		TEAM 21
//		Author 1: Helio Tejeda
//		Author 2: Bohyun Hwang

// Function to move robot (by degree rotation)
void moveRobot(int moveDegree, int speed)
{
	// The speed input determines the direction
	// the robot will move
	//	     positive speed -> forward
	//		   negative speed -> backwards
	moveMotorTarget(motorB, moveDegree, speed);
	moveMotorTarget(motorC, moveDegree, speed);
	waitUntilMotorStop(motorB);
	waitUntilMotorStop(motorC);
}

// Function to spin the robot (by degree rotation)
void spinRobot(int moveDegree, int speed)
{
	// The speed input will be positive for one
	// direction and negative for the other.
	// This will cause the robot to spin in place.
	moveMotorTarget(motorB, moveDegree, -speed);
	moveMotorTarget(motorC, moveDegree, speed);
	waitUntilMotorStop(motorB);
	waitUntilMotorStop(motorC);
}

task main()
{

	// Move robot forward 360 degrees at 100%
	//moveRobot(360, 100);

	// Move robot forward 360 degrees at 10%
	//moveRobot(360, 10);

	// Move robot backward 360 degrees at 100%
	//moveRobot(360, -100);

	// Move robot backward 360 degrees at 10%
	//moveRobot(360, -10);

	// Spin Robot 360 degrees at 100%
	//spinRobot(360, 100);

	// Spin robot at 10% for 2 sec
	spinRobot(360, 10);

}
