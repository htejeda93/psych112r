\normalfont\documentclass[a4paper, 12pt]{article}
\usepackage[a4paper,total={6.5in, 10in}]{geometry}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{comment}
\usepackage[colorlinks]{hyperref}
\usepackage{wrapfig}
\hypersetup{linkcolor=black}
\hypersetup{urlcolor=blue}

\usepackage{verbatim}
\usepackage{listings}
\usepackage{color}

\definecolor{deepgreen}{rgb}{0,0.6,0}
\definecolor{orange}{rgb}{1,0.5,0}
\definecolor{purple}{RGB}{102,0,102}

\renewcommand{\thesubsection}{\textbf{\thesection.\alph{subsection}}}
\newcommand{\tabitem}{$\bullet$  }
\setlength\parindent{0pt}

% C environment
\lstnewenvironment{ccode}[1][]{
	\lstset{
	  language=C,
	  backgroundcolor=\color{white},
	  otherkeywords={task, setMotorSpeed, moveMotorTarget, waitUntilMotorStop, sleep},
	  keywordstyle=\color{blue},
	  emphstyle=\color{purple},
	  stringstyle=\color{deepgreen},
	  commentstyle=\color{deepgreen},
	  showstringspaces=false,
	  %frame=tblr,		% Turn on and off box frame around code
	  #1	% mostly to be used for emph={Class, Function, etc.}
	}
}{}


\title{Midterm Project\\Swiss the Love Candy}
\author{Bohyun Hwang (25868043)\\Heliodoro Tejeda (53604946)}
\date{\today}

\begin{document}

\maketitle \tableofcontents

\clearpage

\section{Overall Strategy and Design}

The task of sorting different types of candies into their appropriate piles, can be broken down into two subtasks. The first of which is simply classification. Can we correctly distinguish between the three items present. The second relates to Swiss Robots and their ability to group and pile objects in a given area.

\subsection{Classification}

We start with the first, classification. For humans, this tasks seems rather trivial; simply look at the item and then classify it based on its color, shape, and/or graphic design. However, for a robot, this task can be rather difficult. For one, robots don’t have eyes like we do and thusly cannot view the different candies. Even if we were to give a robot “eyes”,  it would still have to gather the visual information, process it, and finally determine which item it was seeing. This would take a lot of implementation time as well as ensuring that the visual sensors were working properly (before running), in addition to the computation time (while running) to distinguish the items. Unfortunately for this project, we did not have the means to give our robot such visual sensors which would (aside from the implementation time and computation time) allow us to classify objects easily. Thusly, we were stuck using color sensors as our “eyes” to distinguish between the three different types of candies. What made it even more difficult, was that all of the candies were Valentine’s Day themed, thusly they were all simply different shades of red.

\subsection{Swiss Robots}

The second subtask is related to Swiss Robots. These robots were created to run in an enclosed area with boxes (“trash”) spread around. Their ultimate goal was to cluster these boxes into piles so that it would be easier to clean up the mess. It was this concept that influenced the clustering aspect of our task. However, for this project, since the goal was to cluster items of the same type, a combination of both classification and clustering was to be applied. This made the clustering a little bit harder, as the robot needed to determine which item is was carrying and then group it together with an item of its same type, not just any other item. The implementation of this is explained in further detail in the \textbf{Strategy} section (\textbf{1.d}).

\subsection{Design}

Our design was mostly influenced by the Swiss Robot, however, with a few modifications. One of which was rather than having the sensors angled to the side, they were facing forward. The reason for having the sensors angled to the side on the Swiss Robots was to allow it to push an item directly in front of it; this allowed for the clustering ability. In our case, the clustering was to be a little more advanced, as we only wanted to cluster the same type of item. Thus, we put the sensors facing forward (in the direction the robot would be moving), spread out far enough so that it could sense an item, pick it up, and put it in between the sensors as it searched for another. Having the sensors in front rather than on the side allowed the robot to determine when it would run into an item, then determine which item it was, finally determining the appropriate action to take. In addition, we included an arm in the front that would serve as a bulldozer and bucket. As a bucket, the arm would be able to pick up and hold onto an item until it found another one of the same type. As a bulldozer, the arm would just bulldoze everything that was in front of it. The images below depict the influences of our design.

\begin{figure}[!ht]
	\centering
	\begin{subfigure}{0.45\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{pics/swiss.png}
		\caption{Swiss Robot}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.45\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{pics/bulldozer.png}
		\caption{Bulldozer}
	\end{subfigure}
	\caption{Design Inspiration}
\end{figure}

As seen below, the robot is a vehicle with two large motors for the wheels which allow the robot to move forward, backwards, and turn. In addition to this, it has a third (medium sized) motor in the front which is used for the bulldozer arm. This arm was used to pick up items and store them, essentially allowing the robot to hold an item it had identified and then go about looking for another of the same type. On top of having the three motors, the robot had four sensors; two color sensors and two touch sensors. The color sensors were placed on the floor so that as the robot moved around, it could sense a color change (detecting an item) and then classify it, then determine the action to take. The touch sensors were placed above the color sensors and had an extension added to them so that if it was activated, the robot would either turn in the opposite direction or reverse and turn to avoid the obstacle which activated the sensor. Images of the robot are displayed below.

\begin{figure}[!ht]
	\centering
	\begin{subfigure}{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{pics/robot_arm_down_01.JPG}
		\caption{Robot with Arm Down}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{pics/robot_arm_up_02.JPG}
		\caption{Robot with Arm Up}
	\end{subfigure}
	\caption{Our Robot, Marta}
\end{figure}

\subsection{Strategy}

Our design was what allowed us to implement our strategy as we wished. Having explained the sensors, motors, and their position above, our strategy was simple. We would start our robot, it would then move forward until one of the color sensors sensed an item. We implemented a simple threshold for detecting an item. As the robot was moving around, we noticed that the red, green, and blue (RGB) values picked up by the sensors was always below 5 for each. With this in mind, we put our threshold at 10 for each color, so that if the color sensor picked up a value of at least 10 for either red, green, or blue it would count as an object being detected. This was the first (initial) value sensed. These values were then stored into initial color variables. Then the robot would display these values on the LCD allowing for some delay before the second color reading was taken. Once the second color values were taken, the values for red, green, and blue would be stored in post color variables. Then, based on the change between the first and second color reading, we would determine which item had been picked up by the sensor (this determination is explained in more detail in the Sensor section).\\
\\
After an object had been sensed by the robot, it would then store the type of object it had sensed as a variable, then turn toward the object. Next, it would lift its arm, move forward so that the item would fall inside of its “bucket”, lower its arm, as to simulate the picking up of the item. Next, the robot would turn either left or right and start moving forward, beginning its search for another item.\\
\\
The search for the second item was exactly the same as the search for the first item, however, the action of picking up the item differed. Once a second item was detected, the robot would determine if the sensed item was the same as the item it was carrying. If the items were a match, then the robot would turn to face the item, lift its arm, move forward to group the items together, move backwards, lower its arm, then turn. After turning, the robot would then begin the process again, starting its search for an initial item to pick up. However, it the items were not a match, then the robot would simply turn away from the detected item and continue searching for another item until it found a match.\\
\\
The entire process is simply broken down to a few simple steps. Step one, search for an initial item. Step two, once an item has been found, pick it up and remember the type of item it is. Step three, search for a second item. Step four, if the second item found is the same, drop off the first item with the second and return to step one. If the second item found was not the same type, return to step three.\\
\\
In addition to the simple strategy which came with the color sensors, there were a couple of additions implemented with respect to the touch sensors. The first being that, if at any point the robot was moving forward, if a touch sensor was activated, the robot would move backwards, turn, then continue on. The second being that if an item was detected, as as the robot went to pick it up, a touch sensor was activated, the robot would abort the pick up, reverse, turn around and begin to look for another object. The reason for doing this was that if an object was too close to the wall, it would be impossible to pick it up. In addition to this, because there were white walls on our course, there was a possibility that the robot would detect the wall as an item. Due to this, we wanted to avoid “picking up” an item when the wall was sensed. This second touch sensor use prevented us from doing that.\\
\\
A video report of our robot, the design, strategy, and performance can be found \href{https://www.youtube.com/watch?v=PK-_We0GP3E&feature=youtu.be}{here}.

\pagebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REMEMBER TO LINK VIDEO REPORT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{The Code}

\subsection{Flowchart}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=\textwidth, height=0.8\textheight]{pics/midterm_prog.png}
\end{figure}

\pagebreak

\subsection{Pseudocode}

\begin{figure}[!ht]
	\centering
	\begin{ccode}
Set motor and sensor variables

main()

	clear timer
	reset Arm motor Encoder
	while (less than 3 min)

		initialize first item to 0
		while (less than 3 min)
			set RGB colors to 0
			while (less than 3 min && threshold not passed)
				move forward
				get RGB values
				if (touch sensors pressed)
					move back
					turn around
			stop robot
			determine item to picked up
			pick up item
			turn
			set first item as item found
		end while

		while (less than 3 min)
			set RGB colors to 0
			while (less than 3 min && threshold not passed)
				move forward
				get RGB values
				if (touch sensors pressed)
					move back
					turn around
			stop robot
			determine item found
			if (item found == first item)
				drop off first item
				turn around
				exit loop
			else
				restart loop
		end while
	end while
	stop robot

	\end{ccode}
	\caption{Pseudocode}
\end{figure}


\section{Robot Performance}

\subsection{Sensors}

Due to the fact that we had to distinguish between similarly colored candies, we took various measurements with our color sensor. All of our recorded measurements were taken under normal lighting. To record the color sensor values picked up for each item, we implemented a loop which would move our robot forward until it sensed a color change from its sensor (indicated by a threshold), then stop, display the initial RGB values, pause for another amount of time, then take a second reading of RGB values and display the new values. The robot would then move backwards, pause, then repeat the process until it reached 20 iterations. Our hope was that by taking two readings (one initial and one post), we would be able to more easily distinguish between the different items. The results are displayed below.\\
\\
Note that all values subscripted with an $i$ are initial values, and all values subscripted with a $p$ are the second values picked up by the sensor after a slight delay.

\pagebreak

\begin{figure}[!ht]
	\centering
			\begin{tabular}{|c|c|c|c|c|}
				\hline
				Trial & Red$_i$ & Green$_i$ & Blue$_i$ & $\sum_i$ \\ \hline
				1 & 10 & 7 & 5 & 22 \\ \hline
2 & 10 & 7 & 6 & 23 \\ \hline
3 & 10 & 7 & 6 & 23 \\ \hline
4 & 10 & 7 & 6 & 23 \\ \hline
5 & 10 & 7 & 5 & 22 \\ \hline
6 & 10 & 6 & 5 & 21 \\ \hline
7 & 10 & 6 & 5 & 21 \\ \hline
8 & 10 & 6 & 5 & 21 \\ \hline
9 & 10 & 6 & 5 & 21 \\ \hline
10 & 10 & 6 & 6 & 22 \\ \hline
11 & 10 & 9 & 6 & 25 \\ \hline
12 & 10 & 9 & 6 & 25 \\ \hline
13 & 10 & 6 & 5 & 21 \\ \hline
14 & 10 & 6 & 5 & 21 \\ \hline
15 & 10 & 6 & 5 & 21 \\ \hline
16 & 10 & 6 & 5 & 21 \\ \hline
17 & 10 & 6 & 6 & 22 \\ \hline
18 & 10 & 6 & 6 & 22 \\ \hline
19 & 10 & 6 & 6 & 22 \\ \hline
20 & 10 & 6 & 6 & 22 \\ \hline
Mean & 10.000 & 6.550 & 5.500 & 22.050 \\ \hline
Std & 0.000 & 0.921 & 0.500 & 1.203 \\ \hline

			\end{tabular}
		\caption{Pink Box Initial (Left Sensor)}
\end{figure}

\pagebreak

\begin{figure}[!ht]
			\centering
			\begin{tabular}{|c|c|c|c|c|}
				\hline
				Trial & Red$_i$ & Green$_i$ & Blue$_i$ & $\sum_i$ \\ \hline
				1 & 12 & 8 & 7 & 27 \\ \hline
2 & 13 & 9 & 8 & 30 \\ \hline
3 & 13 & 10 & 8 & 31 \\ \hline
4 & 12 & 9 & 7 & 28 \\ \hline
5 & 12 & 8 & 7 & 27 \\ \hline
6 & 12 & 8 & 7 & 27 \\ \hline
7 & 12 & 8 & 7 & 27 \\ \hline
8 & 13 & 8 & 7 & 28 \\ \hline
9 & 6 & 3 & 2 & 11 \\ \hline
10 & 12 & 8 & 7 & 27 \\ \hline
11 & 11 & 8 & 6 & 25 \\ \hline
12 & 15 & 14 & 10 & 39 \\ \hline
13 & 14 & 9 & 7 & 30 \\ \hline
14 & 13 & 9 & 7 & 29 \\ \hline
15 & 13 & 8 & 7 & 28 \\ \hline
16 & 14 & 9 & 8 & 31 \\ \hline
17 & 13 & 7 & 8 & 28 \\ \hline
18 & 14 & 8 & 9 & 31 \\ \hline
19 & 14 & 9 & 9 & 32 \\ \hline
20 & 12 & 7 & 7 & 26 \\ \hline
Mean & 12.500 & 8.350 & 7.250 & 28.100 \\ \hline
Std & 1.775 & 1.878 & 1.512 & 4.898 \\ \hline

			\end{tabular}
			\caption{Pink Box Post (Left Sensor)}
\end{figure}

\pagebreak

\begin{figure}[!ht]
			\centering
			\begin{tabular}{|c|c|c|c|c|}
				\hline
				Trial & Red$_i$ & Green$_i$ & Blue$_i$ & $\sum_i$ \\ \hline
				1 & 10 & 6 & 4 & 20 \\ \hline
				2 & 10 & 6 & 5 & 21 \\ \hline
				3 & 10 & 6 & 4 & 20 \\ \hline
				4 & 10 & 6 & 5 & 21 \\ \hline
				5 & 10 & 5 & 4 & 19 \\ \hline
				6 & 10 & 5 & 4 & 19 \\ \hline
				7 & 10 & 5 & 4 & 19 \\ \hline
				8 & 10 & 8 & 5 & 23 \\ \hline
				9 & 10 & 8 & 5 & 23 \\ \hline
				10 & 10 & 6 & 5 & 21 \\ \hline
				11 & 10 & 5 & 5 & 20 \\ \hline
				12 & 10 & 6 & 5 & 21 \\ \hline
				13 & 10 & 6 & 5 & 21 \\ \hline
				14 & 10 & 6 & 5 & 21 \\ \hline
				15 & 10 & 6 & 5 & 21 \\ \hline
				16 & 10 & 6 & 4 & 20 \\ \hline
				17 & 10 & 6 & 4 & 20 \\ \hline
				18 & 10 & 7 & 5 & 22 \\ \hline
				19 & 10 & 6 & 5 & 21 \\ \hline
				20 & 10 & 7 & 5 & 22 \\ \hline
				Mean & 10.000 & 6.100 & 4.650 & 20.750 \\ \hline
				Std & 0.000 & 0.831 & 0.477 & 1.135 \\ \hline
			\end{tabular}
			\caption{Pink Box Initial (Right Sensor)}
\end{figure}

\pagebreak

\begin{figure}[!ht]
			\centering
			\begin{tabular}{|c|c|c|c|c|}
				\hline
				Trial & Red$_i$ & Green$_i$ & Blue$_i$ & $\sum_i$ \\ \hline
				1 & 12 & 7 & 5 & 24 \\ \hline
				2 & 12 & 7 & 6 & 25 \\ \hline
				3 & 13 & 8 & 6 & 27 \\ \hline
				4 & 15 & 9 & 7 & 31 \\ \hline
				5 & 7 & 4 & 3 & 14 \\ \hline
				6 & 10 & 6 & 4 & 20 \\ \hline
				7 & 13 & 7 & 6 & 26 \\ \hline
				8 & 13 & 11 & 7 & 31 \\ \hline
				9 & 12 & 10 & 7 & 29 \\ \hline
				10 & 13 & 7 & 6 & 26 \\ \hline
				11 & 12 & 7 & 6 & 25 \\ \hline
				12 & 13 & 8 & 6 & 27 \\ \hline
				13 & 11 & 7 & 5 & 23 \\ \hline
				14 & 12 & 8 & 6 & 26 \\ \hline
				15 & 14 & 9 & 6 & 29 \\ \hline
				16 & 13 & 8 & 6 & 27 \\ \hline
				17 & 16 & 11 & 7 & 34 \\ \hline
				18 & 14 & 9 & 7 & 30 \\ \hline
				19 & 14 & 9 & 7 & 30 \\ \hline
				20 & 12 & 8 & 6 & 26 \\ \hline
				Mean & 12.550 & 8.000 & 5.950 & 26.500 \\ \hline
				Std & 1.830 & 1.612 & 1.023 & 4.225 \\ \hline

			\end{tabular}
			\caption{Pink Box Post (Right Sensor)}
\end{figure}

\pagebreak

\begin{figure}[!ht]
			\centering
			\begin{tabular}{|c|c|c|c|c|}
				\hline
				Trial & Red$_i$ & Green$_i$ & Blue$_i$ & $\sum_i$ \\ \hline
				1 & 11 & 4 & 3 & 18 \\ \hline
				2 & 10 & 6 & 5 & 21 \\ \hline
				3 & 10 & 4 & 2 & 16 \\ \hline
				4 & 11 & 7 & 4 & 22 \\ \hline
				5 & 10 & 5 & 4 & 19 \\ \hline
				6 & 10 & 7 & 10 & 27 \\ \hline
				7 & 10 & 3 & 4 & 17 \\ \hline
				8 & 10 & 4 & 4 & 18 \\ \hline
				9 & 5 & 10 & 2 & 17 \\ \hline
				10 & 10 & 3 & 5 & 18 \\ \hline
				11 & 9 & 10 & 8 & 27 \\ \hline
				12 & 9 & 11 & 7 & 27 \\ \hline
				13 & 9 & 10 & 4 & 23 \\ \hline
				14 & 10 & 10 & 8 & 28 \\ \hline
				15 & 10 & 10 & 1 & 21 \\ \hline
				16 & 9 & 10 & 4 & 23 \\ \hline
				17 & 10 & 3 & 5 & 18 \\ \hline
				18 & 10 & 10 & 4 & 24 \\ \hline
				19 & 10 & 7 & 6 & 23 \\ \hline
				20 & 10 & 7 & 7 & 24 \\ \hline
				Mean & 9.650 & 7.050 & 4.850 & 21.550 \\ \hline
				Std & 1.195 & 2.819 & 2.197 & 3.735 \\ \hline

			\end{tabular}
			\caption{Ninja Turtle Heart Initial (Left Sensor)}
\end{figure}

\pagebreak

\begin{figure}[!ht]
			\centering
			\begin{tabular}{|c|c|c|c|c|}
				\hline
				Trial & Red$_i$ & Green$_i$ & Blue$_i$ & $\sum_i$ \\ \hline
				1 & 3 & 2 & 1 & 6 \\ \hline
				2 & 6 & 2 & 1 & 9 \\ \hline
				3 & 3 & 2 & 2 & 7 \\ \hline
				4 & 2 & 1 & 1 & 4 \\ \hline
				5 & 1 & 1 & 0 & 2 \\ \hline
				6 & 2 & 1 & 1 & 4 \\ \hline
				7 & 5 & 2 & 2 & 9 \\ \hline
				8 & 4 & 1 & 1 & 6 \\ \hline
				9 & 1 & 1 & 0 & 2 \\ \hline
				10 & 3 & 4 & 3 & 10 \\ \hline
				11 & 13 & 15 & 12 & 40 \\ \hline
				12 & 2 & 2 & 1 & 5 \\ \hline
				13 & 9 & 10 & 6 & 25 \\ \hline
				14 & 2 & 2 & 1 & 5 \\ \hline
				15 & 0 & 0 & 0 & 0 \\ \hline
				16 & 1 & 1 & 0 & 2 \\ \hline
				17 & 11 & 12 & 6 & 29 \\ \hline
				18 & 5 & 2 & 3 & 10 \\ \hline
				19 & 4 & 2 & 2 & 8 \\ \hline
				20 & 3 & 1 & 2 & 6 \\ \hline
				Mean & 4.000 & 3.200 & 2.250 & 9.450 \\ \hline
				Std & 3.347 & 3.995 & 2.791 & 9.892 \\ \hline

			\end{tabular}
			\caption{Ninja Turtle Heart Post (Left Sensor)}
\end{figure}

\pagebreak

\begin{figure}[!ht]
			\centering
			\begin{tabular}{|c|c|c|c|c|}
				\hline
				Trial & Red$_i$ & Green$_i$ & Blue$_i$ & $\sum_i$ \\ \hline
				1 & 10 & 7 & 2 & 19 \\ \hline
				2 & 10 & 3 & 4 & 17 \\ \hline
				3 & 10 & 4 & 2 & 16 \\ \hline
				4 & 10 & 2 & 3 & 15 \\ \hline
				5 & 10 & 6 & 2 & 18 \\ \hline
				6 & 10 & 10 & 7 & 27 \\ \hline
				7 & 10 & 2 & 2 & 14 \\ \hline
				8 & 10 & 5 & 4 & 19 \\ \hline
				9 & 10 & 10 & 3 & 23 \\ \hline
				10 & 8 & 2 & 3 & 13 \\ \hline
				11 & 10 & 2 & 4 & 16 \\ \hline
				12 & 10 & 8 & 3 & 21 \\ \hline
				13 & 10 & 6 & 4 & 20 \\ \hline
				14 & 10 & 4 & 1 & 15 \\ \hline
				15 & 10 & 5 & 4 & 19 \\ \hline
				16 & 10 & 2 & 2 & 14 \\ \hline
				17 & 10 & 7 & 5 & 22 \\ \hline
				18 & 11 & 6 & 4 & 21 \\ \hline
				19 & 10 & 10 & 6 & 26 \\ \hline
				20 & 10 & 9 & 6 & 25 \\ \hline
				Mean & 9.950 & 5.500 & 3.550 & 19.000 \\ \hline
				Std & 0.497 & 2.802 & 1.532 & 4.025 \\ \hline
			\end{tabular}
			\caption{Ninja Turtle Heart Initial (Right Sensor)}
\end{figure}

\pagebreak

\begin{figure}[!ht]
			\centering
			\begin{tabular}{|c|c|c|c|c|}
				\hline
				Trial & Red$_i$ & Green$_i$ & Blue$_i$ & $\sum_i$ \\ \hline
				1 & 3 & 1 & 1 & 5 \\ \hline
				2 & 0 & 0 & 0 & 0 \\ \hline
				3 & 2 & 1 & 1 & 4 \\ \hline
				4 & 1 & 1 & 0 & 2 \\ \hline
				5 & 1 & 1 & 0 & 2 \\ \hline
				6 & 2 & 1 & 1 & 4 \\ \hline
				7 & 4 & 2 & 1 & 7 \\ \hline
				8 & 8 & 6 & 4 & 18 \\ \hline
				9 & 3 & 1 & 1 & 5 \\ \hline
				10 & 3 & 2 & 1 & 6 \\ \hline
				11 & 1 & 1 & 0 & 2 \\ \hline
				12 & 4 & 2 & 1 & 7 \\ \hline
				13 & 5 & 3 & 2 & 10 \\ \hline
				14 & 2 & 1 & 0 & 3 \\ \hline
				15 & 5 & 5 & 3 & 13 \\ \hline
				16 & 4 & 2 & 1 & 7 \\ \hline
				17 & 2 & 1 & 0 & 3 \\ \hline
				18 & 4 & 2 & 1 & 7 \\ \hline
				19 & 8 & 9 & 3 & 20 \\ \hline
				20 & 2 & 1 & 0 & 3 \\ \hline
				Mean & 3.200 & 2.150 & 1.050 & 6.400 \\ \hline
				Std & 2.088 & 2.104 & 1.117 & 5.132 \\ \hline

			\end{tabular}
			\caption{Ninja Turtle Heart Post (Right Sensor)}
\end{figure}

\pagebreak

\begin{figure}[!ht]
			\centering
			\begin{tabular}{|c|c|c|c|c|}
				\hline
				Trial & Red$_i$ & Green$_i$ & Blue$_i$ & $\sum_i$ \\ \hline
				1 & 5 & 10 & 10 & 25 \\ \hline
				2 & 10 & 1 & 8 & 19 \\ \hline
				3 & 11 & 3 & 5 & 19 \\ \hline
				4 & 7 & 4 & 10 & 21 \\ \hline
				5 & 10 & 6 & 7 & 23 \\ \hline
				6 & 10 & 6 & 4 & 20 \\ \hline
				7 & 10 & 5 & 5 & 20 \\ \hline
				8 & 11 & 10 & 4 & 25 \\ \hline
				9 & 4 & 9 & 10 & 23 \\ \hline
				10 & 7 & 10 & 10 & 27 \\ \hline
				11 & 4 & 6 & 10 & 20 \\ \hline
				12 & 5 & 7 & 10 & 22 \\ \hline
				13 & 10 & 2 & 4 & 16 \\ \hline
				14 & 4 & 7 & 10 & 21 \\ \hline
				15 & 10 & 1 & 1 & 12 \\ \hline
				16 & 10 & 7 & 4 & 21 \\ \hline
				17 & 10 & 7 & 3 & 20 \\ \hline
				18 & 10 & 1 & 4 & 15 \\ \hline
				19 & 5 & 8 & 10 & 23 \\ \hline
				20 & 4 & 8 & 10 & 22 \\ \hline
				Mean & 7.850 & 5.900 & 6.950 & 20.700 \\ \hline
				Std & 2.707 & 2.948 & 3.057 & 3.422 \\ \hline

			\end{tabular}
			\caption{Other Heart Initial (Left Sensor)}
\end{figure}

\pagebreak

\begin{figure}[!ht]
			\centering
			\begin{tabular}{|c|c|c|c|c|}
				\hline
				Trial & Red$_i$ & Green$_i$ & Blue$_i$ & $\sum_i$ \\ \hline
				1 & 1 & 0 & 0 & 1 \\ \hline
				2 & 2 & 1 & 1 & 4 \\ \hline
				3 & 4 & 2 & 1 & 7 \\ \hline
				4 & 4 & 2 & 2 & 8 \\ \hline
				5 & 5 & 3 & 3 & 11 \\ \hline
				6 & 4 & 2 & 2 & 8 \\ \hline
				7 & 3 & 1 & 0 & 4 \\ \hline
				8 & 2 & 2 & 2 & 6 \\ \hline
				9 & 4 & 3 & 2 & 9 \\ \hline
				10 & 2 & 1 & 1 & 4 \\ \hline
				11 & 4 & 1 & 0 & 5 \\ \hline
				12 & 1 & 1 & 0 & 2 \\ \hline
				13 & 5 & 4 & 3 & 12 \\ \hline
				14 & 1 & 0 & 0 & 1 \\ \hline
				15 & 2 & 0 & 0 & 2 \\ \hline
				16 & 4 & 2 & 2 & 8 \\ \hline
				17 & 4 & 1 & 3 & 8 \\ \hline
				18 & 5 & 4 & 7 & 16 \\ \hline
				19 & 7 & 12 & 14 & 33 \\ \hline
				20 & 1 & 0 & 0 & 1 \\ \hline
				Mean & 3.250 & 2.100 & 2.150 & 7.500 \\ \hline
				Std & 1.639 & 2.567 & 3.182 & 7.039 \\ \hline

			\end{tabular}
			\caption{Other Heart Post (Left Sensor)}
\end{figure}

\pagebreak

\begin{figure}[!ht]
			\centering
			\begin{tabular}{|c|c|c|c|c|}
				\hline
				Trial & Red$_i$ & Green$_i$ & Blue$_i$ & $\sum_i$ \\ \hline
				1 & 10 & 2 & 2 & 14 \\ \hline
				2 & 10 & 7 & 2 & 19 \\ \hline
				3 & 10 & 4 & 2 & 16 \\ \hline
				4 & 10 & 5 & 6 & 21 \\ \hline
				5 & 10 & 7 & 4 & 21 \\ \hline
				6 & 9 & 8 & 10 & 27 \\ \hline
				7 & 10 & 9 & 3 & 22 \\ \hline
				8 & 10 & 6 & 4 & 20 \\ \hline
				9 & 7 & 10 & 3 & 20 \\ \hline
				10 & 10 & 11 & 8 & 29 \\ \hline
				11 & 6 & 10 & 10 & 26 \\ \hline
				12 & 6 & 10 & 9 & 25 \\ \hline
				13 & 8 & 11 & 10 & 29 \\ \hline
				14 & 8 & 10 & 1 & 19 \\ \hline
				15 & 10 & 5 & 1 & 16 \\ \hline
				16 & 10 & 1 & 2 & 13 \\ \hline
				17 & 7 & 10 & 8 & 25 \\ \hline
				18 & 9 & 10 & 9 & 28 \\ \hline
				19 & 10 & 7 & 5 & 22 \\ \hline
				20 & 7 & 11 & 11 & 29 \\ \hline
				Mean & 8.850 & 7.700 & 5.500 & 22.050 \\ \hline
				Std & 1.459 & 2.968 & 3.428 & 4.934 \\ \hline

			\end{tabular}
			\caption{Other Heart Initial (Right Sensor)}
\end{figure}

\pagebreak

\begin{figure}[!ht]
			\centering
			\begin{tabular}{|c|c|c|c|c|}
				\hline
				Trial & Red$_i$ & Green$_i$ & Blue$_i$ & $\sum_i$ \\ \hline
				1 & 1 & 1 & 0 & 2 \\ \hline
				2 & 1 & 0 & 0 & 1 \\ \hline
				3 & 6 & 4 & 1 & 11 \\ \hline
				4 & 5 & 3 & 2 & 10 \\ \hline
				5 & 4 & 2 & 2 & 8 \\ \hline
				6 & 9 & 6 & 2 & 17 \\ \hline
				7 & 8 & 5 & 3 & 16 \\ \hline
				8 & 6 & 7 & 2 & 15 \\ \hline
				9 & 5 & 1 & 0 & 6 \\ \hline
				10 & 2 & 0 & 0 & 2 \\ \hline
				11 & 4 & 5 & 4 & 13 \\ \hline
				12 & 4 & 3 & 1 & 8 \\ \hline
				13 & 3 & 2 & 1 & 6 \\ \hline
				14 & 2 & 0 & 0 & 2 \\ \hline
				15 & 2 & 0 & 0 & 2 \\ \hline
				16 & 7 & 4 & 2 & 13 \\ \hline
				17 & 2 & 1 & 0 & 3 \\ \hline
				18 & 2 & 1 & 1 & 4 \\ \hline
				19 & 12 & 16 & 13 & 41 \\ \hline
				20 & 1 & 1 & 0 & 2 \\ \hline
				Mean & 4.300 & 3.100 & 1.700 & 9.100 \\ \hline
				Std & 2.917 & 3.618 & 2.830 & 8.944 \\ \hline

			\end{tabular}
			\caption{Other Heart Post (Right Sensor)}
\end{figure}

\pagebreak

There were 20 measurements taken for each sensor and for each candy. Thusly, overall we took 120 different color measurements in an attempt to find a pattern that would allow us and our robot to distinguish between the candies solely based on color. Without using any machine learning techniques (which we considered greatly, but did not have enough time to implement); only one obvious pattern emerged.\\
\\
This pattern was that the second measurement taken by the color sensor (for the pink box candy) was always higher (90\% of the time) than the initial measurement. Thusly, this allowed us to (with 90\% certainty) distinguish the pink box from the hearts. Unfortunately, due to lack of time, we were unable to determine a classification algorithm to distinguish between the two hearts (with 80\% or greater probability). So, we settled with only being able to classify the pink box as one group and the two hearts as another.\\
\\
Although all of our recorded measurements are given above, we did take a couple of unrecorded measurements. These measurements were due to the fact that there would be an additional light source on the course. We thought that with higher luminosity, there would be a change in the color values picked up by the sensor. Thusly, we took 2 measurements (per sensor) for each object while shining a light (mimicking the extra light on the course). We found that the color values for each item were raised slightly, however, nothing was so significant, that we decided to not worry about the extra light source.

\subsection{Actuators}

No measurements were recorded for the actuators. For the most part, we knew that our robot would be moving forward as a set speed. The only times it would need precise movement would be when it was turning to face an item, when it was moving forward to pick up and item or drop off an item, and when it was turning. For the most part, these precise movements were implemented using the motor encoders and mostly calculated by eye. In initial degree value was given to the motor encoder function, and if we did not like the result, we would simply add or subtract a bit to the given degree. However, we can say that after we found an acceptable degree value for a given movement, it was very accurate (this is because the motor encoders are fairly reliable).

\subsection{Performance Outside of Class}

Several simulations were run outside of class to test our strategy and performance. Given three candies to take home (one of each), we were limited on the testing we were able to accomplish. Before implementing the complete strategy together, we implemented it by parts. The parts, their descriptions, and tests are explained below.\\
\\
The first part needed was to determine how well our color sensors worked. Therefore, once the robot was finished being designed, a simple color sensor program was written for testing. This program was simple. It would move the robot forward until it sensed an object in front of it (given by the threshold) value. It would then take the initial reading, wait a bit, then take a second reading. Then it would move backwards, reset, and repeat the process. It was here that we determined our threshold value of 10 for each RGB component and how we recorded our sensor tests given in the \textbf{Sensor} section.\\
\\
The second part we implemented were the touch sensors. We wrote a simple program that would back up and turn based on which sensor was activated. For this program, the robot would move forward until a sensor was activated, then execute a desired action. If the left sensor was activated, the robot would reverse, then turn right and continue moving forward. If the right sensor was activated, the robot would reverse, turn left, then continue moving forward. Finally, if both sensors were pressed, the robot would reverse, turn around completely, then continue moving forward. We ran this program several times to ensure that the touch sensors were working properly and that each desired action was taken appropriately.\\
\\
Once both the color sensor and touch sensors programs were finalized, we created a third program combining the two. The robot would move forward until it detected an item, display the item it had detected, then turn and continue on. If at any point a touch sensor was activated, it would complete the desired action based on the touch sensor activated and continue on. After implementing the other two separately and combining them into this one, the program worked swimmingly.\\
\\
This third program was the majority of our strategy. After running it several times, it was able to identify the heart objects correctly 75\% of the time and the pink box correctly 80\% of the time. These numbers were to our satisfaction seeing as we could not find a better way to distinguish between the different types of items. With this done, we moved on to finalizing our program, this included picking up items, storing the values and searching for a matching item.\\
\\
After finishing the final program which included the turning to pick up an item, determining the action based on the second item encountered, and finally using a one minute timer, we had a few mock trials for the competition. These mock trials proved fairly successful as the robot was able to sense an object in front of it correctly around 90\% of the time. The other ten percent only came with the hearts. Being that they were fairly transparent, if the color sensor reached the heart at the right angle, it would pass right through it and would fail to detect the item. When it came to distinguishing between the items correctly, the robot had a success rate of 80\%, with the misclassification of the pink box coming then the sensor picked up the box at an angle (the corner of the box, classifying it as a heart).\\
\\
With these performance metrics outside of class during our own testing, we were satisfied with our robots performance and excited to see how it would perform during the competition.

\subsection{Performance During the Challenge}

During the competition, our robot performed fairly well. Unfortunately, like most robots, our accuracy from testing to in the field performance fell. In our case, it fell from 80\% to roughly around 70\%. Although this doesn’t seem like a drastic loss, it is a loss none the less and fairly frustrating as you are watching your robot wonder the course.\\
\\
We found that most misclassifications came again with the pink box being sensed on the corner of the box. This would lead our robot to classify it as a heart, thusly, continue looking for the wrong object. In addition to the misclassifications, since the competition had many more items than we tested with, the bulldozer worked overtime, picking up an abundance of items at times.\\
\\
Carrying an abundance of items, without having sensed any of them made the robot pick up all of the items in front of it once it did finally sense an item. With this, the robot only knew of the item it had sensed, thusly it would look for a matching item based on that one, ignoring the fact that it was also carrying three of four other items. This process can be viewed in the video report, linked above.\\
\\
However, when everything did go correctly (which was about 70\% of the time), it was great. The pink boxes would be grouped together and the hearts would be separately grouped. There was even one moment in time where the our robot grouped together five pink boxed into a single cluster. This was the highlight of our performance during the competition.\\
\\
Fortunately, there was one aspect that worked exactly the same as it did during our tests. This was the touch sensors. Whenever a touch sensor was activated, the robot executed the proper response. This included sensing the wall as an item then ignoring it as a touch sensor was activated when it went to retrieve the item.

\subsection{Repeatability}

During the competition, our robot had two runs, both of which were a success. In the first run, the robot scored a total of 26 points, with the second run being one point less (25 points). With this, including the consistency during our own test trials, we can safely say that our robot was consistent across runs making it repeatable.\\
\\
There was one slight difference that we implemented in between runs during the competition. This small change, although did not lead to such a huge disparity in points accumulated, was seen through the area covered by our robot. The change that was implemented was changing the turn radius of our robot after retrieving the first item. Initially, after picking up the first item, the robot would turn either left or right roughly 120$^\circ$ from its given position. We thought that this turn radius was a bit high, thusly we lowered it to roughly 90$^\circ$. Although this does not seem like a huge change, there was a significant difference in the area covered by our robot. In the first run, the robot traveled throughout the entire competition area. For the second run, the robot traveled about 50\% of the competition area. This significant difference in area covered is what caused our robot to pick up the same item several times and deposit it in another location. Fortunately for us, although the territory covered was significantly less, the points accumulated did not suffer.

\section{Creativity}

In the beginning, we make the robot put an ultrasonic sensor in the middle to figure objects like candies or wall, and a color sensor, and a touch sensor on each side of arm. However, it was difficult to find the candy and distinguish its color. The robot only could find out when the candy was at the side of its arm with a color sensor. Therefore, we re-designed with two color sensors at the both sides to find and check if the candies exist, and its colors instead of using ultrasonic sensor. Then, we put two touch sensors above the color sensors to do not run into the wall. Moreover, we made a moving arm in the middle of the sensors to grab and hold a candy, and to avoid the candy bounced off. For the software, we basically focused on the goal, which is sort different kinds of candies. We made the color measurement charts that show all color values for each candy to differentiate the candies. Also, we make the robot senses twice using the color sensor when it find a candy, so that the result become more accurate and reliable. We tried to distinguish the three different kinds of candies, but it was hard to distinguish between ninja-heart and the other heart. So, we tried to distinguish at least a box candy and two heart shape candies.

\section{Conclusion}

The robot works well to sort between boxes and two heart shaped candies. It perfectly hold a candy and never let a candy goes away until it would find the other same candy. We measured the color values of three candies at least 25 times for each candy, so that the robot could more accurately recognize and distinguish what candy is it. We were satisfied with the color distinguishing. We made an arm to grab a candy, and removed the ultrasonic sensor because we could not find  enough space to put it. It was perfect to hold a candy, however, we did not have either color sensor or ultrasonic sensor. It caused that our robot could not distinguish, and even notice whether there are candies in the middle. If the robot would have more place to connect with ultrasonic sensor, we want to put it in front part of an arm. Thus, the robot could improve that it can notice if the candy is coming or not in the middle. If there is a candy, it distinguish the candy using either side of the color sensor. Then, the result become more precise to sort candies. \\
\\
\textbf{Three-Constituents Principle}\\
$\bullet$ No, the robot is not directly interact with people. It just automatically move by the RobotC program.\\
\\
\textbf{Complete-Agent}\\
$\bullet$ No, even though the robot move and act automatically, it still needs supports from programmers to improve movements.\\
\\
\textbf{Cheap Design}\\
$\bullet$ No,It has two kinds of sensors, and move carefully and work accurately. It does not care about people, just move by the program.\\
\\
\textbf{Redundancy/Degeneracy}\\
$\bullet$ Yes, the robot can move and find candies using the color sensors. And its uses color sensor to distinguish the objects.\\
\\
\textbf{Sensory-Motor Coordination}\\
$\bullet$ Yes, the robot uses two color sensors to find objects and it will stop when it senses any object. And it also have two touch sensors to bump into the wall.\\
\\
\textbf{Ecological Balance}\\
$\bullet$ The robot is well-balanced with the arm, sensors, and motors, that make it moves smoothly.\\
\\
\textbf{Parallel, Loosely Coupled Processes}\\
$\bullet$ No, the robot does not change the movement according to the situation. It solves the situation only by the given program.\\
\\
\textbf{Value}\\
$\bullet$ No, the robot does not think or move through good or bad. It just helps to sort objects. There is no good or bad.



\end{document}
