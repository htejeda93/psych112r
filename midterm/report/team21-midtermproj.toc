\contentsline {section}{\numberline {1}Overall Strategy and Design}{2}{section.1}
\contentsline {subsection}{\numberline {\textbf {1.a}}Classification}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {\textbf {1.b}}Swiss Robots}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {\textbf {1.c}}Design}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {\textbf {1.d}}Strategy}{3}{subsection.1.4}
\contentsline {section}{\numberline {2}The Code}{5}{section.2}
\contentsline {subsection}{\numberline {\textbf {2.a}}Flowchart}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {\textbf {2.b}}Pseudocode}{6}{subsection.2.2}
\contentsline {section}{\numberline {3}Robot Performance}{7}{section.3}
\contentsline {subsection}{\numberline {\textbf {3.a}}Sensors}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {\textbf {3.b}}Actuators}{20}{subsection.3.2}
\contentsline {subsection}{\numberline {\textbf {3.c}}Performance Outside of Class}{20}{subsection.3.3}
\contentsline {subsection}{\numberline {\textbf {3.d}}Performance During the Challenge}{21}{subsection.3.4}
\contentsline {subsection}{\numberline {\textbf {3.e}}Repeatability}{22}{subsection.3.5}
\contentsline {section}{\numberline {4}Creativity}{22}{section.4}
\contentsline {section}{\numberline {5}Conclusion}{23}{section.5}
